#/bin/bash

Number_of_expected_args=3
E_WRONG_ARGS=85

script_usage="<login> <passwd> <speed>
<speed in kbps> 640 768 896 1.0 1.3 1.7 2.1 3.1 4.1 5.0 5.7 6.4 7.1 7.8 8.5 9.2 10.0 12.0 15.0 max 640)"
if [ $# != $Number_of_expected_args ]
then
  echo "Usage:`basename $0` $script_usage"
  exit $E_WRONG_ARGS
fi 

case $3 in
  640) TARIF="POS-MA4-0003";;
  768) TARIF="POS-MA4-0004";;
  896) TARIF="POS-MA4-0005";;
  1.0) TARIF="POS-MA4-0006";;
  1.3) TARIF="POS-MA4-0007";;
  1.7) TARIF="POS-MA4-0008";;
  2.1) TARIF="POS-MA4-0009";;
  3.1) TARIF="POS-MA4-0010";;
  4.1) TARIF="POS-MA4-0011";;
  5.0) TARIF="POS-MA4-0012";;
  5.7) TARIF="POS-MA4-0013";;
  6.4) TARIF="POS-MA4-0014";;
  7.1) TARIF="POS-MA4-0015";;
  7.8) TARIF="POS-MA4-0016";;
  8.5) TARIF="POS-MA4-0017";;
  9.2) TARIF="POS-MA4-0018";;
  10.0) TARIF="POS-MA4-0019";;
  12.0) TARIF="POS-MA4-0020";;
  15.0) TARIF="POS-MA4-0021";;
  max) TARIF="POS-MA4-0022";;
  640) TARIF="POS-MA4-0003";;
  *) TARIF="POS-MA4-0003";;
esac
pr=`curl -c cook.txt  -s -k -L -d "IDToken1=$1&IDToken2=$2&goto=https%3A%2F%2Fmy.yota.ru%3A443%2Fselfcare%2FloginSuccess&gotoOnFail=https%3A%2F%2Fmy.yota.ru%3A443%2Fselfcare%2FloginError&old-token=&org=customer" https://login.yota.ru/UI/Login | grep "РќРµРІРµСЂРЅРѕРµ РёРјСЏ РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ РёР»Рё РїР°СЂРѕР»СЊ."`

if [ ${#pr} -eq 0 ]
then
  pr=`curl -b cook.txt  -s -k -L https://my.yota.ru/selfcare/devices | grep "\"product\" va"`
if [ ${#pr} -eq 0 ]
then
  echo "Personal cabinet error!!!"
  logger -t YOTA "Personal cabinet error!!!"
  exit 1
fi
else
  echo "Login error!!!"
  logger -t YOTA "Login error!!!"
  exit 1
fi
pr=${pr#*value=\"}
pr=${pr%%\" />*}

OCODE=$TARIF
od=`curl -b cook.txt  -s -k -L -d "product=$pr&offerCode=$OCODE&homeOfferCode=&areOffersAvailable=false&period=&status=custom&autoprolong=0&isSlot=false&resourceId=&currentDevice=1&username=&isDisablingAutoprolong=false" https://my.yota.ru/selfcare/devices/changeOffer | grep "offerDisabled"`

if [ ${#od} -eq 0 ]
then
  echo "ChangeOffer Error!!!"
  logger -t YOTA "ChangeOffer Error!!!"
  exit 1
fi

st=${od%%offerDisabled*}
st=${st%amountNumber*}
st=${st%payInfo\":\"\"*}
st=${st##*:}
remain=${st%,*}

dn=${od%%offerDisabled*}
dn=${dn%amountNumber*}
dn=${dn##*:}
dn=${dn%,*}${remain}
dn=$(sed 's/\"\"/ /g' <<< "$dn")
dn=$"Remain: "${dn} 

od=${od#*offerDisabled}
od=${od%%amountNumber*}
speed=${od#*speedNumber\":}
speed=$"Speed: "${speed%%,*}
speed=$(sed 's/<[^>]*>//g' <<< "$speed")

echo $speed $dn
logger -t YOTA $speed $dn
exit 0